== Задание 2. Создание Docker образов (images)

Создайте свой шаблон образа для nginx. Можете использовать файл Dockerfile из текущего каталога.
Для проверки работоспособности своего Dockerfile собирайте образ используя следующую команду:

[source, bash]
----
docker build -t registry.gitlab.com/cloud-native-development1/module-1/practice/docker/task2:<tag-name> .  <1> <2>
----
<1> [tag-name] используйте свое имя латиницей kebab-case (Aliaksandr Nozdryn-Platnitski a-nozdryn-platnitski).
<2> не забудьте `.` в конце команды, она указывает контекст текущая директория.

Проверить контейнер можно следующей командой:
[source, bash]
----
docker inspect registry.gitlab.com/cloud-native-development1/module-1/practice/docker/task2:<tag-name> <1>
----
<1> [tag-name] используйте свое имя латиницей kebab-case (Aliaksandr Nozdryn-Platnitski a-nozdryn-platnitski).

.Задание
. Используйте в качестве базового образа официальный `nginx` версии `1.18.0` на основе Alpine Linux (`nginx:1.18.0-alpine`)
. Определите переменную окружения `MY_NAME` со значением вашего имени (MY_NAME=AlexNP)
. Назначьте рабочей директорией `/usr/share/nginx/html` - директория со статическими документами по умолчанию
. Скопируйте файл `index.html` из текущего каталог() в каталог `/usr/share/nginx/html`
. Установите утилиту `curl`
. Обозначить что контейнер будет слушать 80 порт
. Запустить основной процесс `nginx` в фоновом режиме (`nginx -g daemon off;`)
